var jsdom = require('jsdom');
const {JSDOM} = jsdom;

const {document} = (new JSDOM('')).window;
global.document = document;

if (typeof window === 'undefined') {
    global.window = {};
    }

if (typeof window !== 'object') {
    global.window = global;
    global.window.navigator = {};
}



require.extensions['.png'] = function () {
    return null;
};
