import React from 'react'
import {expect} from 'chai';
import {shallow} from 'enzyme';

import App from "./index";

describe("App component", function () {
    const wrapper = shallow(<App />);
    it("should be render", function () {
        (expect(wrapper).to.have.length(1));
    });
});
